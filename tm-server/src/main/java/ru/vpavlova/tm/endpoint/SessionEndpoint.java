package ru.vpavlova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.api.endpoint.ISessionEndpoint;
import ru.vpavlova.tm.api.service.dto.ISessionService;
import ru.vpavlova.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Autowired
    private ISessionService sessionService;

    @Override
    @WebMethod
    @SneakyThrows
    public Session openSession(
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password
    ) {
        return sessionService.open(login, password);
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public Session closeSession(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) {
        sessionService.validate(session);
        return sessionService.close(session);
    }

}
