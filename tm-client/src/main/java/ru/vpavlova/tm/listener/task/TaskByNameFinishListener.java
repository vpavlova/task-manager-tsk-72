package ru.vpavlova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.event.ConsoleEvent;
import ru.vpavlova.tm.listener.AbstractTaskListener;
import ru.vpavlova.tm.endpoint.Session;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.util.TerminalUtil;

@Component
public class TaskByNameFinishListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-finish-status-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Finish task status by name.";
    }

    @Override
    @EventListener(condition = "@taskByNameFinishListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER NAME:");
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        @NotNull final String name = TerminalUtil.nextLine();
        taskEndpoint.finishTaskByName(session, name);
    }

}
