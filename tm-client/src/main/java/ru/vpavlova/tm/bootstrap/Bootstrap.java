package ru.vpavlova.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.api.service.ILoggerService;
import ru.vpavlova.tm.component.FileScanner;
import ru.vpavlova.tm.event.ConsoleEvent;
import ru.vpavlova.tm.listener.AbstractListener;
import ru.vpavlova.tm.util.SystemUtil;
import ru.vpavlova.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

@Getter
@Setter
@Component
@NoArgsConstructor
public class Bootstrap {

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    public ILoggerService loggerService;

    @Autowired
    public AbstractListener[] commands;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    public void parseArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return;
        @Nullable String command = null;
        for (@NotNull final AbstractListener listener : listeners) {
            if (arg.equals(listener.arg())) command = listener.name();
        }
        if (command == null) return;
        publisher.publishEvent(new ConsoleEvent(command));
    }

    private void textWelcome() {
        loggerService.debug("TEST!!");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
    }

    private void init() {
        initPID();
        initFileScanner();
    }


    private void initFileScanner() {
        fileScanner.init();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void process() {
        @NotNull String command = "";
        while (!"exit".equals(command)) {
            try {
                command = TerminalUtil.nextLine();
                loggerService.command(command);
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void run(@Nullable final String... args) {
        textWelcome();
        if (parseArgs(args)) System.exit(0);
        init();
        process();
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
