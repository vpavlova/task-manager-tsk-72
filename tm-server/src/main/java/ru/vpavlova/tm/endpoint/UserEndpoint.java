package ru.vpavlova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.api.endpoint.IUserEndpoint;
import ru.vpavlova.tm.api.service.dto.ISessionService;
import ru.vpavlova.tm.api.service.dto.IUserService;
import ru.vpavlova.tm.dto.Session;
import ru.vpavlova.tm.dto.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {
    @Autowired
    private ISessionService sessionService;

    @Autowired
    private IUserService userService;

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public User findUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        sessionService.validate(session);
        return userService.findByLogin(login).orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User findUserOneBySession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        sessionService.validate(session);
        return userService.findOneById(session.getUserId()).orElse(null);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void setPassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        sessionService.validate(session);
        userService.setPassword(session.getUserId(), password);
    }

}
